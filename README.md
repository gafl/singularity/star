# STAR Singularity container
### Bionformatics package STAR<br>
Spliced Transcripts Alignment to a Reference © Alexander Dobin, 2009-2019.<br>
STAR Version: 2.7.3<br>
[https://github.com/alexdobin/STAR]

Singularity container based on the recipe: Singularity.star_2.7.3

Package installation using Miniconda3 V4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build star_v2.7.3.sif Singularity.star_2.7.3`

### Get image help
`singularity run-help ./star_v2.7.3.sif`

#### Default runscript: STAR
#### Usage:
  `star_v2.7.3.sif --help`<br>
    or:<br>
  `singularity exec star_v2.7.3.sif STAR --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull star_v2.7.3.sif oras://registry.forgemia.inra.fr/gafl/singularity/star/star:latest`

